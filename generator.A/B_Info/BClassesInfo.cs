using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;

namespace generator.RenameThisNamespaceA.B_Info
{
    public class BClassesInfo
    {
        public ITypeSymbol domainClassType { get; }
        public ClassDeclarationSyntax classDS { get; }
        public string Name { get; }

        public BClassesInfo(ITypeSymbol type, ClassDeclarationSyntax node)
        {
            domainClassType = type;
            classDS = node;
            Name = GetName();
        }

        private string GetName()
        {
            return classDS.Identifier.ValueText;
        }
    }

    public partial class NodesInfo
    {
        public IEnumerable<BClassesInfo> GetClassesInfo()
        {
            // var foundTypes = new HashSet<ITypeSymbol>();
            foreach (ClassDeclarationSyntax node in _syntaxReceiver.CandidateClasses)
            {
                // SemanticModel semanticModel = compilation.GetSemanticModel(node.SyntaxTree);
                // ITypeSymbol domainClassType = semanticModel.GetTypeInfo(node).Type;

                // if (domainClassType is null || foundTypes.Contains(domainClassType))
                //     continue;

                // foundTypes.Add(domainClassType);
                // yield return new ClassInfo(domainClassType, node);
                yield return new BClassesInfo(null, node);
            }
        }

    }
}