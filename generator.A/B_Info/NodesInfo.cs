using Microsoft.CodeAnalysis;
using generator.RenameThisNamespaceA.A_Syntax;
using System.Collections.Generic;

namespace generator.RenameThisNamespaceA.B_Info
{
    /// <summary>
    /// //////////////// 2ST ////////////////////
    /// This is the second step, before generate.
    /// I get Info for the gathered syntax Nodes
    /// </summary>
    public partial class NodesInfo
    {
        private GeneratorExecutionContext _context;
        private SyntaxReceiver _syntaxReceiver;
        public NodesInfo(GeneratorExecutionContext context, SyntaxReceiver syntaxReceiver) : this()
        {
            _context = context;
            _syntaxReceiver = syntaxReceiver;
        }


        public IEnumerable<BClassesInfo> DomainClasses { get; }

        private NodesInfo()
        {
            DomainClasses = GetClassesInfo();
        }

    }
}