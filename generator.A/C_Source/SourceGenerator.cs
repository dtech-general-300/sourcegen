﻿using Microsoft.CodeAnalysis;
using generator.RenameThisNamespaceA.A_Syntax;
using generator.RenameThisNamespaceA.B_Info;
using System;

namespace generator.RenameThisNamespaceA.C_Source
{
    [Generator]
    public class SourceGenerator : ISourceGenerator
    {
        public void Initialize(GeneratorInitializationContext context)
        {
            context.RegisterForSyntaxNotifications(() => new SyntaxReceiver());
        }

        public void Execute(GeneratorExecutionContext context)
        {
            if (context.SyntaxReceiver is not SyntaxReceiver syntaxReceiver)
                throw new InvalidOperationException("Invalid syntax receiver");

            var info = new NodesInfo(context, syntaxReceiver);

            context.AddDomainClasses(info);
            context.AddAppClasses(info);
        }

    }
}
