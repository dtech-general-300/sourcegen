using Microsoft.CodeAnalysis;
using generator.RenameThisNamespaceA.B_Info;
using System.Text;

namespace generator.RenameThisNamespaceA.C_Source
{
    public static class AddAppClassesExt
    {
        public static void AddAppClasses(this GeneratorExecutionContext context, NodesInfo info)
        {
            
            if (context.Compilation.AssemblyName == "LibraryA")
            {

                var sb = new StringBuilder();
                sb.AppendLine($@"
// auto generated
using dtech.Generator.TRTT;
namespace dtech.Generator.Classes
{{
    /// <summary>
    /// 
    /// (este es) AssemblyName: {context.Compilation.AssemblyName} -
    /// 
    /// </summary>

    public class CakeW {{
        public BrownieY bbb {{get;set;}}
    }}");

                foreach (var classs in info.DomainClasses)
                {
                    sb.AppendLine($"    public partial class Super{classs.Name}Dto {{ }}");
                }
                sb.AppendLine(@"
}");

                context.AddSource("classes.cs", sb.ToString());
            }
        }
    }
}