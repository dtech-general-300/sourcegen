﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;

namespace generator.RenameThisNamespaceA.A_Syntax
{
    public static class AddDomainClassesNodesExt
    {
        public static void AddDomainClassesNodes(this List<ClassDeclarationSyntax> classes, SyntaxNode syntaxNode)
        {
            if (syntaxNode is ClassDeclarationSyntax classSyntax // solo clases
                && classSyntax.Parent is NamespaceDeclarationSyntax namespaceSyntax // dentro de un namespace
                && namespaceSyntax.Name.ToString().Contains("Aggregates") // que su namespace sea un específico
            )
            {
                classes.Add(classSyntax);
            }
        }

    }
}
