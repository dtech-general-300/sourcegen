using Microsoft.CodeAnalysis;
using generator.RenameThisNamespaceB.B_Info;

namespace generator.RenameThisNamespaceB.C_Source
{
    public static class AddDomainClassesExt
    {
        public static void AddDomainClasses(this GeneratorExecutionContext context, NodesInfo info)
        {
            // generate on a specific assembly (dll of project)
            if (context.Compilation.AssemblyName == "LibraryB")
            {

                context.AddSource("DomainClasses.cs", $@"
namespace dtech.Generator.TRTT
{{
    /// <summary>
    /// 
    /// GlobalNamespace: {context.Compilation.GlobalNamespace} -
    /// AssemblyName: {context.Compilation.AssemblyName} -
    /// ReferencedAssemblyNames: {context.Compilation.ReferencedAssemblyNames} -
    /// ToString(): {context.Compilation.ToString()}
    /// 
    /// </summary>
    public class BrownieY {{
        public string minicake {{get;set;}}
    }}
}}");
            }
        }
    }
}