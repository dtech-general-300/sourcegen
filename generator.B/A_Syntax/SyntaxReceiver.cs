﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;

namespace generator.RenameThisNamespaceB.A_Syntax
{
    /// <summary>
    /// //////////////// 1ST ////////////////////
    /// This is the first step, before generate.
    /// I only detect syntaxNodes I'm interested in
    /// </summary>
    public class SyntaxReceiver : ISyntaxReceiver
    {
        public SyntaxReceiver() { }

        public List<ClassDeclarationSyntax> CandidateClasses { get; } = new();

        /// Called for every syntax node in the compilation that change (efficiency),
        /// that is, after all was reviewed at least one time.
        /// We can inspect the nodes and save any information useful for generation
        public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
        {
            CandidateClasses.AddDomainClassesNodes(syntaxNode);
        }
    }
}
